'use strict';

let fila = '';
let piso = 4;
for (let i = 0; i < piso; i++) {
    fila = '*';
    for (let j = 0; j < i; j++) {
        fila = fila + '*';
    }
    console.log(fila);
}

console.log('PIRAMIDE_2');
/* ################
 * ## Pirámide 2 ##
 * ################
 *
 * Crea una función que reciba una altura y dibuje una figura
 * como la que sigue:
 *
 *    1
 *    22
 *    333
 *    4444
 *    55555
 *
 */
const height = 5;

for (let i = 0; i < height; i++) {
    let lineContent = '';
    for (let j = i + 1; j > 0; j--) {
        lineContent += i + 1;
    }
    console.log(lineContent);
}
