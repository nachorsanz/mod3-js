/**
 * ################
 * ## Pirámide 3 ##
 * ################
 *
 * Crea una función que reciba una altura y dibuje una figura
 * como la que sigue:
 *
 *    1
 *    12
 *    123
 *    1234
 *    12345
 *
 */
const height = 5;
let element = '';

for (let lines = 0; lines < height; lines++) {
    element = element + lines;
    console.log(element);
}
