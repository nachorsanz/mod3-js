/* #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Dado el array [3, 4, 13, 5, 6, 8], muestra por consola qué numeros son pares
 * y qué números son impares.
 *
 * Haz lo mismo pero en este caso indica qué números son primos y cuales no.
 *
 * Por último, crea un nuevo array en el que los valores sean el doble del array
 * original.
 *
 */

'use strict';

const nums = [3, 4, 13, 5, 6, 8];
for (const num of nums) {
    if (num % 2 === 0) {
        console.log(`El numero ${num} es par`);
    } else {
        console.log(`El numero ${num} es par`);
    }
}

console.log('ejercicio_2');
for (const num of nums) {
    let esPrimo = false;
    for (let i = 2; i < num; i++) {
        if (num % i === 0) {
            esPrimo = true;
        }
        if (esPrimo === true) {
            console.log('es primo');
        }
        console.log('no es primo');
    }
}
const array_2 = [];
for (const num of nums) {
    array_2.push(num * 2);
}
console.log(array_2);
