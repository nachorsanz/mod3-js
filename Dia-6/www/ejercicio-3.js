/* #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Dados el siguiente array: [true, false, false, false, true, false, false, false, true]
 *
 *  - Los valores positivos son infectados.
 *
 *  - Los infectados transmiten su enfermedad a los no infectados que tienen a los lados.
 *
 * En este caso el resultado debería ser: [true, true, false, true, true, true, false, true, true];
 *
 */

'use strict';

const covid = [true, false, false, false, true, false, false, false, true];
let index = [];

for (let i = 0; i < covid.length; i++) {
    if (covid[i]) {
        index.push(i);
    }
}
console.log(index);
for (const indexs of index) {
    covid[indexs + 1] = true;
    covid[indexs - 1] = true;
}
console.log(covid);
