/* #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Llegó el momento de poner en práctica todo lo aprendido hasta ahora para...
 * ¡¡DESACTIVAR UNA BOMBA!! Tranquilos, no tenemos que ser expertos en explosivos.
 * Se trata de crear un juego en el que damos al usuario 5 intentos para tratar de
 * desactivar la bomba.
 *
 * Estos son los pasos que debes seguir para tratar de conseguir el objetivo:
 *
 *  - Generar un nº aleatorio del 1 al 10. Existe una función en JavaScript que
 *    nos permite generar un nº al azar, ¿por qué no investigas un poco?
 *
 *  - Una vez hayamos generado el nº de desactivación daremos al usuario un total
 *    de 5 intentos para tratar de averiguar el nº en cuestión.
 *
 *  - Si acierta detenemos el bucle (con un break) y mostramos un mensaje que indica
 *    que la bomba ha sido desactivada. De lo contrario indicamos que la bomba ha explotado.
 */

'use strict';

// 1. Generar un nº entero aleatorio del 1 al 10 (la contraseña para desactivar la bomba).

getRandomInt(1, 10);
function getRandomInt(min, max) {
    let num = Math.floor(Math.random() * (max - min)) + min;
    console.log(num);
    alert('Pulsa para empezar el juego.' + '\nDispones de 5 intentos.');
    let valor = Number(prompt('Introduce un número de 1 a 10'));
    let intentos = 1;
    if (valor === num) {
        alert(`HAS GANADO el número era: ${num}`);
    } else if (valor !== num && intentos <= 5) {
        for (intentos = 1; intentos < 5; intentos += 1) {
            valor = Number(
                prompt(`DAME OTRO NUMERO \nHas gastado ${intentos} intentos`)
            );
            if (valor === num) {
                alert(`HAS GANADO el número era: ${num}`);
                break;
            } else if (valor !== num && intentos >= 4) {
                alert(`GAME OVER el número era: ${num}`);
            }
        }
    }
}

// 2. Damos al usuario 5 intentos para desactivar la bomba.

// 3. Permitimos al usuario insertar un nº.

// 4. Comprobamos si el nº insertado por el usuario es igual a la contraseña.

// 5. Si es así, finalizamos el bucle y mostramos un mensaje diciendo que la bomba se ha desactivado.

// 6. De lo contrario continuamos con el siguiente intento.

// 7. Si no hay más intentos, mostramos un mensaje diciendo que la bomba ha explotado.
