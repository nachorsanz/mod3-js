/* #################
 * ## Ejercicio 8 ##
 * #################
 *
 * - Cuenta el número de letras "r" en el siguiente fragmento de texto:
 *   "Tres tristes tigres tragan trigo en un trigal."
 *
 * - Ahora cuenta las "t". Debes contar las mayúsculas y las minúsculas.
 *
 * - Sustituye todas las "e" por "i".
 *
 */

'use strict';

const animales = 'Tres tristes tigres tragan trigo en un trigal';

let counter = 0;
const letterR = 'r';
for (let i = 0; i < animales.length; i++) {
    if (letterR === animales[i]) {
        counter++;
    }
}
console.log(counter);
const letter = 't';

function text(text, letter, caseSensitive) {
    let counter = 0;
    if (!caseSensitive) {
        text = text.toLowerCase();
    }
    for (let i = 0; i < text.length; i++) {
        if (letter === text[i]) {
            counter++;
        }
    }
    return counter;
}
const animales2 = animales.replaceAll('e', 'i');
/* console.log(text(animales, 'T', true)); */
console.log(animales2);
