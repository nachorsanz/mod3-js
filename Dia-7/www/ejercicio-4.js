/**
 * #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Crea una función que reciba una cadena de texto y la muestre poniendo
 * el signo – entre cada carácter sin usar el método replace ni replaceAll.
 *
 * Por ejemplo, si tecleo "hola qué tal", deberá salir "h-o-l-a- -q-u-e- -t-a-l".
 *
 */

'use strict';
const text = 'hola que tal';
const array = text.split('');

console.log(array);
const newString = array.join('-');
console.log(newString);
