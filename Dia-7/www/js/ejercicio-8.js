/**
 * #################
 * ## Ejercicio 8 ##
 * #################
 *
 * Ordena un array de números de menor a mayor sin usar la función sort().
 *
 */

'use strict';

const nums = [12, 4, 1, 19, 8];
