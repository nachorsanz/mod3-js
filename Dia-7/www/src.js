'use strict';
let myString = ' Metodos con String ';
console.log(myString.repeat(3));
console.log(myString.replace('o', 'i'));

// para pasar a letra minuscula (myString.toLoweCase()); // toUpperCase()
// En este caso estamos buscando la palabra "string". Si existe, será sustituída por l

// palabra "arrays". Si "strings" se repitiera dos veces, este método solo cambiaría l

// primera coincidencia que encuentre.
let newString = myString.replace('String', 'arrays');
console.log(newString);
// Esto nos devuelve un array con todos los caracteres del string por separado.
console.log(myString.split(''));
// Esto nos devuelve un array con los tres primeros caracteres del string recorrido.
console.log(myString.split('', 3));
// Esto nos devuelve un array en el que separa el string donde haya un espacio.
console.log(myString.split(' '));
// Vamos a de nir una hora del día.
const time = '22:30';
// Así podríamos separar las horas de los minutos.
console.log(time.split(':'));
// Nos devuelve un nuevo string que va desde la posición 9 hasta el nal.
console.log(myString.slice(9));
// Nos devuelve un nuevo string que va de la posición 1 hasta la 7. La posición
// de n, en este caso 8, no se incluye.
console.log(myString.slice(1, 8));
//######METODOS CON ARRAYS######

const fruits = ['manzana', 'pera', 'plátano', 'pera', 'naranja'];
// Nos devolverá un 5 dado que en este array hay 5 elementos. Eso no quiere decir
//que la última
// posición sea la [5], la última posición es la [4] dado que la primera posición de un
//array SIEMPRE
// será la posición [0].
console.log(fruits.length); // 5
// Eliminamos la posición 2 del array y la almacenamos en una constante.
const spliceItems = fruits.splice(2, 1);
console.log(spliceItems); // "plátano"
// Como hemos eliminado la posición 2 ha desaparecido del array inicial.
console.log(fruits); // ["naranja", "pera", "pera", "manzana"]
// Agregamos dos nuevas frutas en la posición 1 del array.
fruits.splice(1, 0, 'uva', 'cereza');
// Aquí tenemos las nuevas frutas agregadas.
console.log(fruits); // ["naranja", "uva", "cereza", "pera", "pera", "manzana"]
