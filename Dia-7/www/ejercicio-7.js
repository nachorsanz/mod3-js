/* #################
 * ## Ejercicio 7 ##
 * #################
 *
 * Crea un programa que compruebe si un string es palíndromo, es decir, que se lee igual
 * del derecho que del revés. Por ejemplo, "Arriba la birra" es un palíndromo.
 *
 */

'use strict';

const myText = 'Arriba la birra';
const myText2 = myText.replaceAll(' ', '').toLowerCase().split('').join('');

const myTextReverse = myText
    .replaceAll(' ', '')
    .toLowerCase()
    .split('')
    .reverse()
    .join('');
console.log(myText2);
console.log(myTextReverse);
console.log(myText2 === myTextReverse);
