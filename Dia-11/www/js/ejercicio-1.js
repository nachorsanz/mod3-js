'use strict';

/**
 *  =======================
 *  ··· P E R S O N A S ···
 *  =======================
 */
const persons = [
    {
        name: 'Pedro',
        age: 35,
        code: 'ES',
        infected: true,
        petName: 'Troski',
    },
    {
        name: 'Elisabeth',
        age: 14,
        code: 'UK',
        infected: true,
        petName: 'Firulais',
    },
    {
        name: 'Pablo',
        age: 25,
        code: 'ES',
        infected: false,
        petName: 'Berritxu',
    },
    {
        name: 'Angela',
        age: 18,
        code: 'DE',
        infected: false,
        petName: 'Noodle',
    },
    {
        name: 'Boris',
        age: 50,
        code: 'UK',
        infected: true,
        petName: 'Leon',
    },
    {
        name: 'Donald',
        age: 69,
        code: 'US',
        infected: false,
        petName: 'Pence',
    },
    {
        name: 'Pepito',
        age: 36,
        code: 'ES',
        infected: false,
        petName: 'Carbón',
    },
];

/**
 *  =======================
 *  ··· M A S C O T A S ···
 *  =======================
 */
const pets = [
    {
        petName: 'Troski',
        type: 'perro',
    },
    {
        petName: 'Firulais',
        type: 'perro',
    },
    {
        petName: 'Berritxu',
        type: 'loro',
    },
    {
        petName: 'Noodle',
        type: 'araña',
    },
    {
        petName: 'Leon',
        type: 'gato',
    },
    {
        petName: 'Pence',
        type: 'perro',
    },
    {
        petName: 'Carbón',
        type: 'gato',
    },
];

/**
 *  =======================
 *  ··· A N I M A L E S ···
 *  =======================
 */
const animals = [
    {
        type: 'perro',
        legs: 4,
    },
    {
        type: 'araña',
        legs: 8,
    },
    {
        type: 'gato',
        legs: 4,
    },
    {
        type: 'loro',
        legs: 2,
    },
    {
        type: 'gallina',
        legs: 2,
    },
];

/**
 *  ===================
 *  ··· P A Í S E S ···
 *  ===================
 */
const countries = [
    {
        code: 'CN',
        name: 'China',
        population: 1439,
        infected: 81999,
    },
    {
        code: 'US',
        name: 'Estados Unidos',
        population: 331,
        infected: 112468,
    },
    {
        code: 'DE',
        name: 'Alemania',
        population: 83,
        infected: 56202,
    },
    {
        code: 'ES',
        name: 'España',
        population: 46,
        infected: 72248,
    },
    {
        code: 'UK',
        name: 'Reino Unido',
        population: 67,
        infected: 17301,
    },
];

/**
 *  ###########################
 *  ## E J E R C I C I O   1 ##
 *  ###########################
 *
 *  Número total de infectados del array de personas.
 *
 */
const person = persons.filter((person) => person.infected === true);
const totalInfected = person.length;
console.log(totalInfected);

/**
 *  ###########################
 *  ## E J E R C I C I O   2 ##
 *  ###########################
 *
 *  Número total de infectados en el array de países.
 *
 */

const infected3 = countries.map((infected) => infected.infected);
const countries2 = infected3.reduce((acc, num) => {
    return (acc += num);
}, 0);
console.log(countries2);
/**
 *  ###################3########
 *  ## E J E R C 2I C I O   3 ##
 *  ###########################
 *
 *  País con más infectados.
 *
 */
const moreInfected = countries.map((infected) => infected.infected);
function getMax(array) {
    let result = 0;
    for (const value of array) {
        if (value > result) result = value;
    }
    return result;
}
const max = getMax(moreInfected);
const index = moreInfected.findIndex((value) => value === max);
const country = countries[index].name;
console.log(country);
/**
 *  ###########################
 *  ## E J E R C I C I O   4 ##
 *  ###########################
 *
 *  Array con el nombre de todas las mascotas.
 *
 */
/* const pets2 = pets.filter((value) => value.petName);
const petsName = pets2.map((names) => names.petName); */

const petsName = persons.map((person) => person.petName);
console.log(petsName);

/**
 *  ###########################
 *  ## E J E R C I C I O   5 ##
 *  ###########################
 *
 *  Array de españoles con perro.
 *
 */
const result = persons.filter((value) => {
    return pets.find((animal) => {
        return animal.petName === value.petName && animal.type === 'perro';
    });
});
console.log(result);
/**
 *  ###########################
 *  ## E J E R C I C I O   6 ##
 *  ###########################
 *
 *  Array con las personas. A mayores, este array debe incluír el objeto con los datos de su mascota
 *
 *  {
 *      name: 'Pedro',
 *      age: 35,
 *      country: 'ES',
 *      infected: true,
 *      pet: {
 *          name: 'Troski',
 *          type: 'perro',
 *      }
 *  }
 *
 */
const personsWithAnimal = persons.map((person) => {
    return {
        name: person.name,
        age: person.age,
        country: person.code,
        infected: person.infected,
        pet: pets.find((animal) => {
            return person.petName === animal.petName;
        }),
    };
});

console.log(personsWithAnimal);
/**
 *  ###########################
 *  ## E J E R C I C I O   7 ##
 *  ###########################
 *
 *  Número total de patas de las mascotas de las personas.
 *
 */
const animales = persons.filter((people) => {
    return pets.find((animal) => {
        return animal.petName === people.petName;
    });
});
const types = pets.filter((pets) => {
    return animals.find((animals) => {
        return pets.type === animals.type;
    });
});
const legs2 = types.map((type) => {
    return {
        type: type.type,
        legs: animals.reduce((animals) => {
            return animals.legs === pets.type;
        }),
    };
});

console.log(legs2);

/**
 *  ###########################
 *  ## E J E R C I C I O   8 ##
 *  ###########################
 *
 *  Array con las personas que tienen animales de 4 patas
 *
 */

/**
 *  ###########################
 *  ## E J E R C I C I O   9 ##
 *  ###########################
 *
 *  Array de países que tienen personas con loros como mascota.
 *
 */

/**
 *  #############################
 *  ## E J E R C I C I O   1 0 ##
 *  #############################
 *
 *  Número de infectados totales (en el array de países) de los países con mascotas de ocho patas.
 *
 */
