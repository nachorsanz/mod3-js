'use strict';

Array.prototype.myReduce = function (callback, accumulator) {
    // En principio partimos de la posición 0.
    let i = 0;

    // Si accumulator no recibe ningún valor...
    if (!accumulator) {
        // Asignamos al acumulador lo que haya en la posición 0 del array.
        accumulator = this[0];
        // Cambiamos el valor de i para que el for empiece en el index 1.
        i = 1;
    }

    for (i; i < this.length; i++) {
        // En cada repetición almacenamos en el acumulador lo que devuelva
        // el callback.
        accumulator = callback(accumulator, this[i], i, this);
    }

    return accumulator;
};

const numbers = [2, 5, 4, 23];

const result = numbers.myReduce((acc, num) => {
    return (acc += num);
});

console.log(result);
