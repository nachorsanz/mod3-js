/* #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Crea un programa que simule las tiradas de un dado:
 *
 *      - El dado debe generar en cada tirada un valor entre 1 y 6 (incluídos).
 *
 *      - Cuando el total de tiradas supere o iguale los 100 puntos muestra
 *        un mensaje indicando que se han alcanzado los 100 puntos.
 *
 */
const dice = () => Math.ceil(Math.random() * 6);
let total = 0;
for (let i = 0; total <= 100; i += dice()) {
    const diceValue = dice();
    total += diceValue;
    console.log(`Tirada ${i}, ha salido un ${diceValue}. 
    Tienes un total de ${total} punots.`);
}
