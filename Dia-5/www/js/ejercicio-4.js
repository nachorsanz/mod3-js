/* #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Escribe un programa que permita al usuario introducir elementos en un array.
 * El programa finalizará cuando el usuario introduzca el string "fin", y se
 * mostrará por consola el contenido del array.
 *
 */
'use strict';
// array al  que vamos pusheando elementos
const result = [];

let userContent;
// usamos un do while xq mientras que no fin prompt y push
do {
    // dejamos que meta cosas
    userContent = prompt('Dame cosas ');
    // pusheamos el contenido de userContent al array
    result.push(userContent);
} while (userContent !== 'fin');

// al dejar de pedir datos mostramos el contenido
console.log(result);
