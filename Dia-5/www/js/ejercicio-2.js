/* #################
 * ## Ejercicio 2 ##
 * #################
 *
 * Tenemos 3 equipos de baloncesto. Estos son los resultados de cada equipo
 * en los últimos 4 partidos:
 *
 *      - Equipo A: 35, 46, 29, 58
 *      - Equipo B: 46, 72, 26, 36
 *      - Equipo C: 38, 62, 47, 44
 *
 * Muestra por consola el equipo con la mejor media en estos cuatro partidos.
 * Debes mostrar el nombre del equipo y la media de dicho equipo.
 *
 */
'use strict';

function ordenarNumeroMayor() {
    let equipoA = (35 + 46 + 29 + 58) / 4;
    let equipoB = (46 + 72 + 26 + 36) / 4;
    let equipoC = (38 + 62 + 47 + 44) / 4;

    if (equipoA >= equipoB && equipoA >= equipoC) {
        console.log(`Equipo A es el ganador y su media es de ${equipoA}`);
    } else if (equipoB >= equipoA && equipoB >= equipoC) {
        console.log(`Equipo B es el ganador y su media es de ${equipoB}`);
    } else {
        console.log(`Equipo C es el ganador y su media es de ${equipoB}`);
    }
}
ordenarNumeroMayor();
