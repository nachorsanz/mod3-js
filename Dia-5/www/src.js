'use strict';
console.log(sayHello());
// declaracion de funcion
function sayHello() {
    return 'hola';
}
// expresion de fincion
//se recomienda hacer const
const sayBye = function () {
    return 'adios';
};
console.log(sayBye());
// arrow function
const sayNothing = () => {
    return ' . . . . .';
};
console.log(sayNothing());
// con retunr implicito
const sayNothingB = (a, b) => a * b;
console.log(sayNothingB(3, 2));
// arrays
console.log('arrays');
const numbers = [3, 56, 14, 9];
for (let i = 0; i < 4; i++) {
    console.log(numbers[i]);
}
