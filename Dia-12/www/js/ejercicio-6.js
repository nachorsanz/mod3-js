/**
 * #################
 * ## Ejercicio 6 ##
 * #################
 *
 * Dada la función "simpleMode(numsArray)" tome el aprámetro numsArray y devuelva el número
 * que aparece con más frecuencia.
 *
 * Por ejemplo: si numsArray contiene [10, 4, 5, 2, 4] la salida debería ser 4. Si hay existen
 * dos números que se repiten el mismo número de veces devuelve el que apareció primero en el
 * array (por ejemplo [5, 10, 10, 6, 5] debería devolver 5 porque apareció primero).
 *
 * Si no hay ningún modo, devuelve -1. El array no estará vacío.
 *
 */

'use strict';
const array = [6, 10, 6, 6, 10, 10, 10];

function simpleMode(numsArray) {
    let counter = 1;
    let maxCounter = 0;
    let numRep;
    for (let i = 0; i < numsArray.length; i++) {
        for (let j = i + 1; j < numsArray.length; j++) {
            if (array[i] === array[j]) counter++;
        }
        if (counter > maxCounter) {
            maxCounter = counter;
            numRep = array[i];
        }
        counter = 1;
    }
    if (maxCounter === 0) return -1;
    return numRep;
}

console.log(simpleMode(array));
