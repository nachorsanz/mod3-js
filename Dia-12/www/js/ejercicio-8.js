/**
 * #################
 * ## Ejercicio 8 ##
 * #################
 *
 * Dado el siguiente tablero (un array de arrays) haz las modificaciones necesarias para
 * lograr esto:
 *
 *     - Figura 1:
 *
 *      [
 *          ['X', '-', '-', '-', 'X'],
 *          ['-', 'X', '-', 'X', '-'],
 *          ['-', '-', 'X', '-', '-'],
 *          ['-', 'X', '-', 'X', '-'],
 *          ['X', '-', '-', '-', 'X']
 *      ];
 *
 *
 *     - Figura 2:
 *
 *      [
 *          ['X', 'X', 'X', 'X', 'X'],
 *          ['X', '-', '-', '-', 'X'],
 *          ['X', '-', '-', '-', 'X'],
 *          ['X', '-', '-', '-', 'X'],
 *          ['X', 'X', 'X', 'X', 'X']
 *      ];
 *
 */

'use strict';
const board = [
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-'],
];

/* function conversor1(board) {
 for (let i = 0; i < board.length; i++) {
 for (let j = 0; j < board[i].length; j++) {
 if (i === j) {
 board[i][j] = 'x';
 let valor = board[i].length - 1 - j;
 board[i][valor] = 'x';
 }
 }
 }
 console.table(board);
 return;
 }
 /*conversor1(board);*/
/*  function conversor2(board) {
 for (let i = 0; i < board.length; i++) {
 for (let j = 0; j < board[i].length; j++) {
 if (i === 0 || i === board.length - 1) {
 board[i][j] = 'x';
 } else {
 board[i][0] = 'x';
 board[i][board.length - 1] = 'x';
 }
 }
 }
 return board;
 }
 console.table(conversor2(board)); */
