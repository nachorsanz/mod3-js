/* #################
 * ## Ejercicio 1 ##
 * #################
 *
 * Crea el objeto persona con las propiedades name y age. A mayores, crea un método que reciba un
 * array de animales y genere una nueva propiedad favoriteAnimals que almacene dicho array.
 *
 * Crea un segundo método que permita modificar cualquier propiedad del objeto. Este método debe
 * recibir dos argumentos, el nombre de la propiedad en formato string, y el valor que queremos
 * meter en la misma.
 *
 */

'use strict';
const person = {
    name: 'pepe',
    age: 35,
    favAnimals: function (animals) {
        this.favoriteAnimals = animals;
    },
};
const animals = ['perro', 'gato', 'raton'];

person.favAnimals(animals);
console.log(person);
