/**
 * #################
 * ## Ejercicio 6 ##
 * #################
 *
 *   1. Obtén todos los coches de la marca Audi.
 *
 *   2. Obtén una lista con todos los colores de los coches de marca BMW.
 *
 *   3. Obtén la media de precio de los coches de marca Ford.
 *
 *   4. Obtén un array con las distintas marcas de coches (no se pueden repetir).
 *
 *   5. Obtén un array con los coches de transmisión manual y de color negro.
 *
 *   6. Obtén la suma total de todos los precios.
 *
 */

'use strict';

const coches = [
    {
        marca: 'BMW',
        modelo: 'Serie 3',
        year: 2012,
        precio: 30000,
        puertas: 4,
        color: 'Blanco',
        transmision: 'automatico',
    },
    {
        marca: 'Audi',
        modelo: 'A4',
        year: 2018,
        precio: 40000,
        puertas: 4,
        color: 'Negro',
        transmision: 'automatico',
    },
    {
        marca: 'Ford',
        modelo: 'Mustang',
        year: 2015,
        precio: 20000,
        puertas: 2,
        color: 'Blanco',
        transmision: 'automatico',
    },
    {
        marca: 'Audi',
        modelo: 'A6',
        year: 2010,
        precio: 35000,
        puertas: 4,
        color: 'Negro',
        transmision: 'automatico',
    },
    {
        marca: 'BMW',
        modelo: 'Serie 5',
        year: 2016,
        precio: 70000,
        puertas: 4,
        color: 'Rojo',
        transmision: 'automatico',
    },
    {
        marca: 'Mercedes Benz',
        modelo: 'Clase C',
        year: 2015,
        precio: 25000,
        puertas: 4,
        color: 'Blanco',
        transmision: 'automatico',
    },
    {
        marca: 'Chevrolet',
        modelo: 'Camaro',
        year: 2018,
        precio: 60000,
        puertas: 2,
        color: 'Rojo',
        transmision: 'manual',
    },
    {
        marca: 'Ford',
        modelo: 'Mustang',
        year: 2019,
        precio: 80000,
        puertas: 2,
        color: 'Rojo',
        transmision: 'manual',
    },
    {
        marca: 'Dodge',
        modelo: 'Challenger',
        year: 2017,
        precio: 40000,
        puertas: 4,
        color: 'Blanco',
        transmision: 'automatico',
    },
    {
        marca: 'Audi',
        modelo: 'A3',
        year: 2017,
        precio: 55000,
        puertas: 2,
        color: 'Negro',
        transmision: 'manual',
    },
    {
        marca: 'Dodge',
        modelo: 'Challenger',
        year: 2012,
        precio: 25000,
        puertas: 2,
        color: 'Rojo',
        transmision: 'manual',
    },
    {
        marca: 'Mercedes Benz',
        modelo: 'Clase C',
        year: 2018,
        precio: 45000,
        puertas: 4,
        color: 'Azul',
        transmision: 'automatico',
    },
    {
        marca: 'BMW',
        modelo: 'Serie 5',
        year: 2019,
        precio: 90000,
        puertas: 4,
        color: 'Blanco',
        transmision: 'automatico',
    },
    {
        marca: 'Ford',
        modelo: 'Mustang',
        year: 2017,
        precio: 60000,
        puertas: 2,
        color: 'Negro',
        transmision: 'manual',
    },
    {
        marca: 'Dodge',
        modelo: 'Challenger',
        year: 2015,
        precio: 35000,
        puertas: 2,
        color: 'Azul',
        transmision: 'automatico',
    },
    {
        marca: 'BMW',
        modelo: 'Serie 3',
        year: 2018,
        precio: 50000,
        puertas: 4,
        color: 'Blanco',
        transmision: 'automatico',
    },
    {
        marca: 'BMW',
        modelo: 'Serie 5',
        year: 2017,
        precio: 80000,
        puertas: 4,
        color: 'Negro',
        transmision: 'automatico',
    },
    {
        marca: 'Mercedes Benz',
        modelo: 'Clase C',
        year: 2018,
        precio: 40000,
        puertas: 4,
        color: 'Blanco',
        transmision: 'automatico',
    },
    {
        marca: 'Audi',
        modelo: 'A4',
        year: 2016,
        precio: 30000,
        puertas: 4,
        color: 'Azul',
        transmision: 'automatico',
    },
];
console.log('1. Obtén todos los coches de la marca Audi.');

// 1. Obtén todos los coches de la marca Audi.
const audi = coches.filter((value) => value.marca === 'Audi');
console.log(audi);
console.log('#############');
console.log(
    '2. Obtén una lista con todos los colores de los coches de marca BMW.'
);

// 2. Obtén una lista con todos los colores de los coches de marca BMW.
const bmw = coches.filter((value) => value.marca === 'BMW');
const colorCoche = bmw.map((colored) => colored.color);
console.log(colorCoche);
console.log('#############');
console.log('3. Obtén la media de precio de los coches de marca Ford.');
// 3. Obtén la media de precio de los coches de marca Ford.
const ford = coches.filter((value) => value.marca === 'Ford');
const precioCoche = ford.map((price) => price.precio);
console.log(precioCoche);
const media = ford.reduce((accumulator, ford) => {
    return accumulator + ford.precio;
}, 0);
const result = media / precioCoche.length;
console.log(result);
console.log('#############');
console.log(
    '4. Obtén un array con las distintas marcas de coches (no se pueden repetir).'
);

// 4. Obtén un array con las distintas marcas de coches (no se pueden repetir).

const brand = coches.map((brand) => brand.marca);
const unicos = brand.reduce((accArr, valor) => {
    if (accArr.indexOf(valor) < 0) {
        accArr.push(valor);
    }
    return accArr;
}, []);
console.log(unicos);
console.log('#############');
console.log(
    '5. Obtén un array con los coches de transmisión manual y de color negro.'
);

// 5. Obtén un array con los coches de transmisión manual y de color negro.
const trans = coches.filter((value) => value.transmision === 'manual');
const trans2 = trans.filter((colorin) => colorin.color === 'Negro');
console.log(trans2);
console.log('#############');
console.log('6. Obtén la suma total de todos los precios.');

// 6. Obtén la suma total de todos los precios.
const total = coches.filter((value) => value.precio);
const total2 = total.map((price) => price.precio);
const media2 = total.reduce((accumulator, total3) => {
    return accumulator + total3.precio;
}, 0);
console.log(media2);
console.log('#############');
