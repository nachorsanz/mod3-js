'use strict';

const numbers = [2, 4, 13, 24];
// seguimos el estandar de forof dando como parametro el valor del array recorrido
// el segundo hace referencia a la posicion y hay un tercer que no se usa que hace
//referencia al array completo
const myMapCallback = (num, index, array) => {
    console.log(num);
    console.log(index);
    console.log(array);
    console.log('##########');
};
const result = numbers.map(myMapCallback);
console.log(result);
