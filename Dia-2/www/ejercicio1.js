'use strict';
const nombre = 'Nacho';
let numeroFavorito = 4;

console.log(
    'Mi nombre es: ' + nombre,
    '\nY mi numero favorito es: ' + numeroFavorito
);

numeroFavorito = 25;

console.log(
    'Mi nombre es: ' + nombre,
    '\nY mi numero favorito es: ' + numeroFavorito
);

console.log('     Los tipos de variable son: ');

console.log(
    'Mi nombre es una variable de tipo: ' + typeof nombre,
    '\nY mi numero favorito es una variable de tipo: ' + typeof numeroFavorito
);
