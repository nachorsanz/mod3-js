/* #################
 * ## Ejercicio 4 ##
 * #################
 *
 * Crea una calculadora con "switch" que opere con dos números. La calculadora
 * utilizará la variable "option" para decidir el tipo de operación a realizar.
 * Debe poder sumar, restar, multiplicar y dividir. A mayores debe permitir
 * elevar el nº A a la potencia de B.
 *
 * Bonus point: no se puede dividir entre 0.
 */
const num_c = 4;
const num_d = 0;
let option2 = 3;
console.log('EJERCICIO 4');

switch (option2) {
    case 0:
        console.log(num_c + num_d);
        break;
    case 1:
        console.log(num_c - num_d);
        break;
    case 2:
        console.log(num_c * num_d);
        break;
    case 3:
        if (num_d !== 0) {
            console.log(num_c / num_d);
        } else {
            console.log('No puedes dividir por Cero');
        }
        break;
    case 4:
        console.log(num_c ** num_d);
        break;

    default:
        console.error('Operacion fuera de rango');
        break;
}
