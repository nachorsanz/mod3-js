/* #################
 * ## Ejercicio 3 ##
 * #################
 *
 * Crea una calculadora con "if" que opere con dos números. La calculadora
 * utilizará la variable "option" para decidir el tipo de operación a realizar.
 * Debe poder sumar, restar, multiplicar y dividir. A mayores debe permitir
 * elevar el nº A a la potencia de B.
 *
 * Bonus point: no se puede dividir entre 0.
 */
'use strict';
const num_a = 4;
const num_b = 0;
let option = 3;
console.log('EJERCICIO 3');

if (option === 0) {
    console.log(num_a + num_b);
} else if (option === 1) {
    console.log(num_a - num_b);
} else if (option === 2) {
    console.log(num_a * num_b);
} else if (option === 3) {
    if (num_b !== 0) {
        console.log(num_a / num_b);
    } else {
        console.log('No puedes dividir por cero');
    }
} else if (option === 4) {
    console.log(num_a ** num_b);
} else {
    console.error('Operacion no contemplada');
}
