'use strict';
/* let age = 10;
age += 5;

console.log(age); */

let animal = 2;

/* if (animal === 0) {
    console.log('perro');
} else if (animal === 1) {
    console.log('gato');
} else if (animal === 2) {
    console.log('raton');
} else {
    console.log('no hay animal');
} */

// ejemplo de switch

switch (animal) {
    case 0:
        console.log('perro');
        break;
    case 1:
        console.log('gato');
        break;
    case 2:
        console.log('raton');
        break;
    default:
        console.log('no se reconoce el animal');
}
